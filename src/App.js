import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {
    CartesianGrid,
    Legend,
    ResponsiveContainer,
    Scatter,
    ScatterChart,
    Tooltip,
    Area,
    XAxis,
    BarChart,
    Bar,
    AreaChart,
    YAxis,
    LineChart,
    Line
} from 'recharts'
import moment from 'moment'

class App extends Component {
    render() {
        const data = [
            {name: 'Page A', a: 4000, b: 2400, c: 2400, d: 3400},
            {name: 'Page B', a: 3000, b: 1398, c: 2210, d: 1483},
            {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
            {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
            {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
            {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
            {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
        ];
        return (
            <div className="App">
                <div className="header space-between">
                    <div>
                        <label className="header-title primary">Sunchat</label>
                        <label className="header-title secondary"> Feedback</label>
                    </div>

                    <div >
                        <div className="options">
                            <label className="option active">Vista general</label>
                            <label className="option">Vista general</label>
                            <label className="option">Vista general</label>
                        </div>

                        <div className="avatar">
                            <label className="header-user-name">Jerome Houston</label>
                            <img className="avatar-img" src="https://readytricks.com/wp-content/uploads/2018/01/happy-girl-picture-1.jpg"/>
                        </div>
                    </div>


                </div>

                <div className="horizontal">

                    <div className="side-menu">

                    </div>

                    <div className="page-wrapper">
                        <div>
                            <p className="title">Data overview</p>
                            <div style={{marginBottom: 35}} className="horizontal space-between">

                                <div className="rectangle"></div>
                                <div className="rectangle"></div>
                                <div className="rectangle"></div>
                                <div className="rectangle"></div>

                            </div>
                        </div>

                        <div className="chart-section chart-position ">
                            <div>
                                <p className="chart-title">Calidad de agente</p>
                                <LineChart width={600} height={300} data={data}
                                           margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                                    <XAxis dataKey="name"/>
                                    <YAxis/>
                                    <CartesianGrid strokeDasharray="3 3"/>
                                    <Tooltip/>
                                    <Legend/>
                                    <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{r: 8}}/>
                                    <Line type="monotone" dataKey="uv" stroke="#82ca9d"/>
                                    <Line type="monotone" dataKey="amt" stroke="#82ca9d"/>

                                </LineChart>
                            </div>

                            <div>
                                <p className="chart-title">Calidad de agente</p>
                                <BarChart width={600} height={300} data={data}
                                          margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                                    <CartesianGrid strokeDasharray="3 3"/>
                                    <XAxis dataKey="name"/>
                                    <YAxis/>
                                    <Tooltip/>
                                    <Legend/>
                                    <Bar name="jj" dataKey="pv" fill="#8884d8"/>
                                    <Bar dataKey="uv" fill="#82ca9d"/>
                                </BarChart>
                            </div>
                        </div>

                        <div style={{marginTop: 35, height: 250}} className="chart-section horizontal space-between">
                        </div>


                    </div>
                </div>



                {/*<header className="App-header">*/}
                {/*<img src={logo} className="App-logo" alt="logo" />*/}
                {/*<h1 className="App-title">Welcome to React</h1>*/}
                {/*</header>*/}
                {/*<p className="App-intro">*/}
                {/*To get started, edit <code>src/App.js</code> and save to reload.*/}
                {/*</p>*/}


            </div>
        );
    }
}

export default App;
