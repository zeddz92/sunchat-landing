import React, {Component} from 'react';
import './Landing.css';


class App extends Component {
    render() {
        return (
            <div className="main-container">
                <div className="horizontal center">
                    <img className="check-mark-img" src="checkmark.svg"/>
                    <p className="success-text">Su comentario fue enviado con éxito</p>
                </div>

                <div className="banner">
                    <div className="horizontal center banner-position">
                        <div className="horizontal slogan-container">
                            <img className="holding-phone-img" src="/holding-phone.png"/>
                            <div className="slogan-container-text">
                                <label>The best app for <br/> <b>Sales Promotions</b></label>
                                <p>Marketing-focused analytics platform, also includes app-store analytics in
                                    its <br/> “console” and has a free trial period available. </p>
                            </div>

                        </div>

                        <div className="app-links-container">
                            <label>Download the App</label>
                            <div style={{marginTop: 15}} className="horizontal">

                                <img style={{marginRight: 15}} src="/apple-store.svg"/>
                                <img src="/play-store.svg"/>
                            </div>
                        </div>
                    </div>


                </div>

                <div className="middle-body">
                    <div className="horizontal page-body">
                        <div >
                            <label className="benefits-text">Beneficios de registrarte</label>
                            <p className="benefits-description">Marketing-focused analytics platform, also includes
                                app-store <br/>analytics in its “console” and has a free trial period available. </p>

                            <div>

                                <div className="bullet-point horizontal">
                                    <div className="circle"/>
                                    <label className="bullet-point-text">Marketing-focused analytics platform, also
                                        includes app-<br/>store analytics in its “console” and has a free trial
                                        period<br/>
                                        available. </label>
                                </div>

                                <div className="bullet-point horizontal">
                                    <div className="circle"/>
                                    <label className="bullet-point-text">Marketing-focused analytics platform, also
                                        includes app-<br/>store analytics in its “console” and has a free trial
                                        period<br/>
                                        available. </label>

                                </div>

                                <div className="bullet-point horizontal">
                                    <div className="circle"/>
                                    <label className="bullet-point-text">Marketing-focused analytics platform, also
                                        includes app-<br/>store analytics in its “console” and has a free trial
                                        period<br/>
                                        available. </label>

                                </div>

                            </div>
                        </div>

                        <div className="form">
                            <input placeholder="Nombre"/>
                            <input placeholder="Teléfono o Celular"/>
                            <input placeholder="Contraseña"/>

                            <button>REGISTRARME</button>

                        </div>

                        <div>

                        </div>


                    </div>
                </div>

                <div className="footer">
                    <div className="horizontal">
                        <div className="app-links-container">
                            <label style={{fontSize: 16, fontWeight: 'bold'}}>Download the App</label>
                            <div style={{marginTop: 10}} className="horizontal">

                                <img style={{marginRight: 15}} src="/apple-store.svg"/>
                                <img src="/play-store.svg"/>
                            </div>
                        </div>

                        <div>
                            <label className="footer-title">Follow us</label><br/>
                            <label className="stay-in-touch-text">Stay in touch in our social profiles</label>

                            <div className="social-links horizontal">
                                <img src="/facebook.png"/>
                                <img src="/twitter.png"/>
                                <img src="/instagram.png"/>

                            </div>
                        </div>
                    </div>
                    <div style={{textAlign: 'right', width: '30%'}}>
                        <img className="footer-sunchat-logo" src="/sunchat-footer.png"/>
                        <div className="privacy-container">
                            <a>Term of use</a>
                            <label> | </label>
                            <a>Privacy Policy</a>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default App;
